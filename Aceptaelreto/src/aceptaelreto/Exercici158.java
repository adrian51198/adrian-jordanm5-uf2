package aceptaelreto;

import java.util.Scanner;

public class Exercici158 {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int casos;
		int muros;
		int valor;
		int aux = 0;
		int sa = 0; // saltos arriba
		int ca = 0; // abajo
		int i = 0;
		int j = 0;
		casos = reader.nextInt();
		if (casos > 0 && casos <= 1000000000) {
			while (i < casos) {
				j = 0;
				sa = 0;
				ca = 0;
				aux = 0;
				muros = reader.nextInt();
				while (j < muros) {
					valor = reader.nextInt();
					if (aux == 0) {
						aux = valor;
					} else if (aux > valor) {
						ca++;
					} else if (aux < valor) {
						sa++;

					}
					aux = valor;
					j++;
				}
				System.out.println(sa + " " + ca);
				i++;
			}
		}

		reader.close();
	}
}