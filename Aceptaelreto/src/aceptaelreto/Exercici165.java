package aceptaelreto;

import java.util.Scanner;

public class Exercici165 {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int numero = 0;
		int contp = 0; // comptador pars
		int conti = 0; // comptador impars

		while (numero >= 0) {
			numero = reader.nextInt();
			while (numero != -1) {
				if (numero % 2 == 0) {
					contp++;
				} else {
					conti++;
				}
			}
			if(conti !=0) {
				System.out.print("NO");
			}else {
				System.out.print("SI");
			}
		}

		reader.close();
	}
}