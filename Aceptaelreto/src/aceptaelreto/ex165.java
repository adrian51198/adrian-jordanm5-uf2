package aceptaelreto;

import java.util.Scanner;

public class ex165 {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int number;
		int i;
		int d;
		int impar;
		String numb;

		number = reader.nextInt();
		while (number >= 0) {
			impar = 0;
			numb = String.valueOf(number);
			for (i = numb.length() - 1; i >= 0; i--) {
				d = number / (int) Math.pow(10, i);

				if (d % 2 != 0) {
					impar = 1;
				}
			}
			if (impar != 0) {
				System.out.println("NO");
			} else {
				System.out.println("SI");
			}
			number = reader.nextInt();
		}
		reader.close();
	}

}