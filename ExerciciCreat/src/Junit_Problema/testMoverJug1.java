package Junit_Problema;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import Problema.Combate;

@RunWith(Parameterized.class)

public class testMoverJug1 {
	private String[][] secreto;
	private int fila;
	private int col;
	private int[] stats;

	public testMoverJug1(String[][] secreto, int fila, int col, int[] stats) {
		this.secreto = secreto;
		this.fila = fila;
		this.col = col;
		this.stats = stats;
	}

	@Parameters
	public static Collection<Object[]> numeros() {
		return Arrays.asList(new Object[][] {
				
				 {new String[][]{{"PS","PS","PV"},{"PA","AL","PV"}, {"AL","PS","PA"}},0, 0,new int[]{5,0,0}},
				 {new String[][]{{"PV","PA","PA"},{"AL","PV","PS"}, {"AL","AL","PA"}},2, 1,new int[]{5,5,5}},
				 {new String[][]{{"PA","PA","PA"},{"PA","PA","PA"}, {"PS","PS","PS"}},1, 2,new int[]{0,0,5}},
				 {new String[][]{{"PV","PV","PS"},{"PS","AL","PV"}, {"PV","PS","PA"}},2, 0,new int[]{0,5,0}},
				 {new String[][]{{"PA","PS","PA"},{"PV","PV","PV"}, {"PS","AL","PA"}},0, 2,new int[]{5,0,0}}
		});
	}

	@Test
	public void testStat() {
		int[] stat = Combate.comprobarStats1(secreto, fila, col, stats);
		assertEquals(stat, stats);
		// fail("Not yet implemented");
	}

}
