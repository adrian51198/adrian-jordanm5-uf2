package Junit_Problema;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import Problema.Combate;

@RunWith(Parameterized.class)

public class testSumaJug2 {

	private int[] stats;
	private int[] statsbase;

	public testSumaJug2(int[] stats, int[] statsbase) {
		this.stats = stats;
		this.statsbase = statsbase;
	}

	@Parameters
	public static Collection<Object[]> numeros() {
		return Arrays.asList(new Object[][] 
				{
				{new int[]{20,20,27}, new int[] {120,20,32}},
				{new int[]{3,2,5}, new int[] {103,2,10}},
				{new int[]{30,20,20}, new int[] {130,20,20}},
				{new int[]{50,25,10}, new int[] {50,25,10}},
				{new int[]{2,20,30}, new int[] {102,20,35}},
				});
	}
	@Test
	public void testSumaStat() {
		int[] stat = Combate.sumaStatsJug2(stats, statsbase);
		assertEquals(stat, statsbase);
		// fail("Not yet implemented");
	}

}