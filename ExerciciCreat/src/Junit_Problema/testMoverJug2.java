package Junit_Problema;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import Problema.Combate;


@RunWith(Parameterized.class)

public class testMoverJug2 {
	private String[][] secreto;
	private int fila;
	private int col;
	private int[] stats;

	public testMoverJug2(String[][] secreto, int fila, int col, int[] stats) {
		this.secreto = secreto;
		this.fila = fila;
		this.col = col;
		this.stats = stats;
	}

	@Parameters
	public static Collection<Object[]> numeros() {
		return Arrays.asList(new Object[][] {
				 {new String[][]{{"AL","PA","PS"},{"PA","PS","AL"}, {"PA","AL","AL"}},0, 1,new int[]{5,5,5}},
				 {new String[][]{{"PV","PV","PA"},{"AL","PV","PA"}, {"PS","PV","PV"}},2, 2,new int[]{0,5,0}},
				 {new String[][]{{"PA","PA","PA"},{"PA","PA","PA"}, {"PS","PS","PS"}},1, 1,new int[]{0,0,5}},
				 {new String[][]{{"PV","PV","PS"},{"AL","AL","PV"}, {"AL","PS","PA"}},2, 0,new int[]{5,5,5}},
				 {new String[][]{{"PA","PS","PS"},{"PA","PV","PS"}, {"PA","PV","PA"}},1, 2,new int[]{5,0,0}}
		});
	}

	@Test
	public void testStat() {
		int[] stat = Combate.comprobarStats2(secreto, fila, col, stats);
		assertEquals(stat, stats);
		// fail("Not yet implemented");
	}

}
