package Junit_Problema;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import Problema.Combate;

@RunWith(Parameterized.class)

public class testSumaJug1 {

	private int[] stats;
	private int[] statsbase;

	public testSumaJug1(int[] stats, int[] statsbase) {
		this.stats = stats;
		this.statsbase = statsbase;
	}

	@Parameters
	public static Collection<Object[]> numeros() {
		return Arrays.asList(new Object[][] 
				{
				{new int[]{10,5,10}, new int[] {110,5,15}},
				{new int[]{15,5,5}, new int[] {115,5,10}},
				{new int[]{5,5,5}, new int[] {105,5,10}},
				{new int[]{100,0,0}, new int[] {200,0,5}},
				{new int[]{100,20,2}, new int[] {200,20,7}},
				});
	}
	@Test
	public void testSumaStat() {
		int[] stat = Combate.sumaStatsJug1(stats, statsbase);
		assertEquals(stat, statsbase);
		// fail("Not yet implemented");
	}

}
