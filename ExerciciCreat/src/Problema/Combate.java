package Problema;

import java.util.Scanner;

/**
 * <h2>Juego en el cual su objetivo es que uno de los dos jugadores caiga
 * derrotado. Estos jugadores ir�n cogiendo estad�sticas por el tablero y
 * despues de eso se generar� una batalla autom�tica, ganar� quien haya elegido
 * mejor sus movimientos y tenga m�s stats, ya que eso har� que su vida y ataque
 * sean superior</h2>
 * 
 * Busca informaci�n de Javadoc en <a href="http://www.google.com">GOOGLE</a>
 * 
 * @see <a href="http://www.google.com">Google</a>
 * @version 1-2017
 * @author Adrian Jordan
 * @since 17-02-2020
 */

public class Combate {
	static Scanner reader = new Scanner(System.in);

	public static void main(String[] args) {
		jugar();
	}

	/**
	 * Esta funcion sirve para iniciar el tablero donde todas las posiciones seran
	 * "ocultas" menos J1 Y J2. Por aqui ir�n moviendose los jugadores.
	 * 
	 * @return Te delvolver� el tablero inicializado
	 */
	public static String[][] tableroIniciar() {
		String tablero[][] = new String[10][10];
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[i].length; j++) {
				if (i == 0 && j == 0) {
					tablero[i][j] = "J1";
				} else {
					if (i == 9 && j == 9) {
						tablero[i][j] = "J2";
					} else {
						tablero[i][j] = " #";
					}
				}
			}
		}
		return tablero;
	}

	/**
	 * Esta funcion basicamente hace lo mismo que iniciarTablero, pero aqui est�n
	 * todas las stats ocultas para que los dos jugadores vayan cogiendo y sumando a
	 * sus estadisticas base.
	 * 
	 * @return te devuelve el tablero con las estadisticas.
	 */
	public static String[][] iniciarStats() {
		String secreto[][] = { { "ND", "PS", "PV", "PA", "PS", "ND", "ND", "PS", "PV", "ND" },
				{ "PV", "PA", "PV", "ND", "PA", "PV", "PS", "ND", "ND", "PS" },
				{ "ND", "PS", "ND", "ND", "ND", "ND", "ND", "PA", "PV", "PS" },
				{ "AL", "ND", "PA", "ND", "PS", "PA", "ND", "ND", "ND", "ND" },
				{ "ND", "ND", "ND", "AL", "ND", "ND", "PS", "ND", "PV", "PS" },
				{ "PS", "PV", "ND", "PA", "PV", "ND", "ND", "PV", "ND", "PA" },
				{ "ND", "ND", "ND", "PS", "ND", "AL", "ND", "PS", "ND", "ND" },
				{ "PV", "ND", "PS", "ND", "ND", "PS", "PA", "ND", "PS", "PV" },
				{ "PV", "PA", "ND", "ND", "PA", "PS", "PV", "ND", "PA", "PV" },
				{ "PA", "ND", "PA", "PV", "ND", "AL", "PA", "ND", "PS", "ND" } };
		return secreto;
	}

	/**
	 * En este metodo el jugador 1 podr� ejecutar sus movimientos y ir� moviendose
	 * por el tablero, tambi�n llamaremos a otro metodo/funcion donde comprobaremos
	 * las estadisticas que hay en la posici�n donde esta actualmente dicho jugador.
	 * 
	 * @param tablero le pasamos el tablero para poder ir moviendose a la direcci�n
	 *                que le han dicho por teclado.
	 * @param secreto este parametro ser� para otro metodo donde se comprobara que
	 *                estadistica hay en el tablero
	 * @param stats1  esta parametro es igual que el anterior, se usar� para otra
	 *                funcion que llamamos, para asi ir sumando las estadisticas
	 *                necesarias.
	 */
	public static void movimientoJug1(String[][] tablero, String[][] secreto, int[] stats1) {
		char dir;
		int mov = 0;
		int fila = 0, col = 0;
		do {
			mov++;
			dir = reader.next().charAt(0);
			dir = Character.toLowerCase(dir);
			if (dir == 'w') {
				tablero[fila - 1][col] = tablero[fila][col];
				tablero[fila][col] = " #";
				fila = fila - 1;
			} else if (dir == 's') {
				tablero[fila + 1][col] = tablero[fila][col];
				tablero[fila][col] = " #";
				fila = fila + 1;
			} else if (dir == 'a') {
				tablero[fila][col - 1] = tablero[fila][col];
				tablero[fila][col] = " #";
				col = col - 1;
			} else if (dir == 'd') {
				tablero[fila][col + 1] = tablero[fila][col];
				tablero[fila][col] = " #";
				col = col + 1;
			}
			comprobarStats1(secreto, fila, col, stats1);
		} while (mov < 8);
	}

	/**
	 * Este metodo es igual que el movimientoJug1, pero en este caso es para el
	 * jugador 2, retornamos los mismos parametros para exactamente hacer lo mismo
	 * pero a las estadisticas de este jugador.
	 * 
	 * @param tablero retorna el tablero donde diremos en que posicion se mueve
	 * @param secreto comprobacion de que estadistica hay en dicha posicion
	 * @param stats2  sumar la estadistica
	 */
	public static void movimientoJug2(String[][] tablero, String[][] secreto, int[] stats2) {
		char dir;
		int mov = 0;
		int fila = 9, col = 9;
		do {
			mov++;
			dir = reader.next().charAt(0);
			dir = Character.toLowerCase(dir);
			if (dir == 'w') {
				tablero[fila - 1][col] = tablero[fila][col];
				tablero[fila][col] = " #";
				fila = fila - 1;
			} else if (dir == 's') {
				tablero[fila + 1][col] = tablero[fila][col];
				tablero[fila][col] = " #";
				fila = fila + 1;
			} else if (dir == 'a') {
				tablero[fila][col - 1] = tablero[fila][col];
				tablero[fila][col] = " #";
				col = col - 1;
			} else if (dir == 'd') {
				tablero[fila][col + 1] = tablero[fila][col];
				tablero[fila][col] = " #";
				col = col + 1;
			}
			comprobarStats2(secreto, fila, col, stats2);
		} while (mov < 8);
	}

	/**
	 * Aqui es donde haremos la comprobaci�n de las estadisticas del jugador 1, yo
	 * he hecho un vector de 3 posiciones ya que en mis jugadores habr� 3 tipos de
	 * estadisticas diferentes (SALUD,VELOCIDAD,ATAQUE) cabe decir que estas no son
	 * las estadisticas base, si no las que van cogiendo por el tablero
	 * 
	 * @param secreto este parametro se le pasa pare comprobar que estadistica en
	 *                esa casilla.
	 * @param fila    habr� que pasarle la posicion donde el jugador se ha movido en
	 *                este caso la fila.
	 * @param col     habr� que pasarle la posicion donde el jugador se ha movido en
	 *                este caso la columna
	 * @param stats1  este parametro hace que sumemos la estadistica en la posici�n
	 *                necesaria del vector
	 * @return stats1 Retornamos el valor de las estadisticas cogidas por el tablero del jugador 1
	 */
	public static int[] comprobarStats1(String[][] secreto, int fila, int col, int[] stats1) {

		if (secreto[fila][col].equals("PS")) {
			stats1[0] += 5;
		} else if (secreto[fila][col].equals("PV")) {
			stats1[1] += 5;
		} else if (secreto[fila][col].equals("PA")) {
			stats1[2] += 5;
		} else if (secreto[fila][col].equals("AL")) {
			stats1[0] += 5;
			stats1[1] += 5;
			stats1[2] += 5;
		}
		return stats1;
	}

	/**
	 * Este metodo es igual que comprobarStats1 pero para el jugador 2
	 * 
	 * @param secreto este parametro se le pasa pare comprobar que estadistica en
	 *                esa casilla.
	 * @param fila    habr� que pasarle la posicion donde el jugador se ha movido en
	 *                este caso la fila.
	 * @param col     habr� que pasarle la posicion donde el jugador se ha movido en
	 *                este caso la columna
	 * @param stats2  este parametro hace que sumemos la estadisticas en la posici�n
	 *                necesaria del vector
	 * @return stats2 Retornamos el valor de las estadisticas cogidas por el tablero del jugador 2
	 */
	public static int[] comprobarStats2(String[][] secreto, int fila, int col, int[] stats2) {
		if (secreto[fila][col].equals("PS")) {
			stats2[0] += 5;
		} else if (secreto[fila][col].equals("PV")) {
			stats2[1] += 5;
		} else if (secreto[fila][col].equals("PA")) {
			stats2[2] += 5;
		} else if (secreto[fila][col].equals("AL")) {
			stats2[0] += 5;
			stats2[1] += 5;
			stats2[2] += 5;
		}
		return stats2;
	}

	/**
	 * En este caso si que son las estadisticas base del jugador 1, y aqui si que se
	 * sumaran las estadasticas base y las que ha cogido por el tablero
	 * anteriormente.
	 * 
	 * @param stats1 le pasamamos las estadisticas cogidas para sumarselas a las
	 *               estadisticas base       
	 * @param statsbase1 le pasaremos el vector vacio de statsbase1.  
	 * @return lo que retorna este metodo es el total de las estadisticas del
	 *         jugador 1
	 */
	public static int[] sumaStatsJug1(int[] stats1,int[]statsbase1) {
		statsbase1[0] = 100;
		statsbase1[1] = 0;
		statsbase1[2] = 5;
		statsbase1[0] += stats1[0];
		statsbase1[1] += stats1[1];
		statsbase1[2] += stats1[2];
		return statsbase1;
	}

	/**
	 * Utilizaremos la misma base que sumaStatsJug1, pero ahora cogeremos las
	 * estadisticas que ha ido cogiendo el jugador 2
	 * 
	 * @param stats2 estadisticas cogidas por el tablero del jugador 2
	 * @param statsbase2 le pasaremos el vector vacio de statsbase2.  
	 * @return retornamos el total de las estadisticas del jugador 2
	 */
	public static int[] sumaStatsJug2(int[] stats2,int[]statsbase2) {
		statsbase2[0] = 100;
		statsbase2[1] = 0;
		statsbase2[2] = 5;
		statsbase2[0] += stats2[0];
		statsbase2[1] += stats2[1];
		statsbase2[2] += stats2[2];
		return statsbase2;
	}

	/**
	 * Aqui es donde una vez los dos jugadores hayan efectuado todos sus movimientos
	 * se ejecutar� la partida, ganar� el que mejor estadisticas tiene. Tambi�n he
	 * hecho que el que ataca primero es el jugador que tiene m�s velocidad ya que
	 * si no esa estadistica ser�a tonta.
	 * 
	 * @param statsjug1 total stats jug1
	 * @param statsjug2 total stats jug2
	 * @param statsbase1 le pasaremos el vector de statsbase1 para el metodo sumastasJug1.  
	 * @param statsbase2 le pasaremos el vector de statsbase2 para el metodo sumastasJug2.  
	 */
	public static void turnos(int[] statsjug1, int[] statsjug2,int[]statsbase1,int[]statsbase2) {
		int[] totaljug1 = new int[3];
		int[] totaljug2 = new int[3];
		int ronda = 0;
		totaljug1 = sumaStatsJug1(statsjug1,statsbase1);
		totaljug2 = sumaStatsJug2(statsjug2,statsbase2);
		if ((totaljug1[1] > totaljug2[1]) || (totaljug1[1] == totaljug2[1])) {
			do {
				ronda++;
				totaljug2[0] = totaljug2[0] - totaljug1[2];
				if (totaljug2[0] > 0) {
					totaljug1[0] = totaljug1[0] - totaljug2[2];
				}
			} while (totaljug1[0] > 0 && totaljug2[0] > 0);
		} else {
			do {
				ronda++;
				totaljug1[0] = totaljug1[0] - totaljug2[2];
				if (totaljug1[0] > 0) {
					totaljug2[0] = totaljug2[0] - totaljug1[2];
				}
			} while (totaljug1[0] > 0 && totaljug2[0] > 0);
		}
		if (totaljug1[0] > totaljug2[0]) {
			System.out.println("HA GUANYAT EL GOHAN A LA RONDA " + ronda);
		} else {
			System.out.println("HA GUANYAT EL CELL A LA RONDA " + ronda);
		}
	}

	/**
	 * Metodo que servir� para poner a 0 las estadisticas (no las base) de los dos
	 * jugadores, simplemente es para si se juega m�s de una partida que no se
	 * queden las que en la anterior partida hemos cogido.
	 * 
	 * @param stats1 servir� para poner a 0 stats jug1
	 * @param stats2 servir� para poner a 0 stats jug2
	 */
	public static void reiniciarStats(int[] stats1, int[] stats2) {
		stats1[0] = 0;
		stats1[1] = 0;
		stats1[2] = 0;

		stats2[0] = 0;
		stats2[1] = 0;
		stats2[2] = 0;
	}

	/**
	 * Este metodo es el principal ya que aqui ser� corrido todo el juego, se
	 * preguntar� al usuario que cuantos casos quiere que haya, se efectur�n las
	 * movimientos de los dos jugadores, y despues de todo se iniciar� la batalla
	 * automatica. Por ultimo tocar� reiniciar stats para jugar la siguiente.
	 */
	public static void jugar() {
		int[] stats1 = new int[3];
		int[] stats2 = new int[3];
		int[] statsbase1 = new int[3];
		int[] statsbase2 = new int[3];
		String tablero[][];
		String secreto[][];
		int partidas = 0;
		partidas = reader.nextInt();
		reader.nextLine();
		for (int i = 0; i < partidas; i++) {
			tablero = tableroIniciar();
			secreto = iniciarStats();
			movimientoJug1(tablero, secreto, stats1);
			movimientoJug2(tablero, secreto, stats2);
			turnos(stats1, stats2,statsbase1,statsbase2);
			reiniciarStats(stats1, stats2);
		}

	}
}
